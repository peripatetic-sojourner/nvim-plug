To install:

    git clone https://gitlab.com/peripatetic-sojourner/nvim-plug ~/.nvim


    cat > ~/.nvimrc <<EOF
    set runtimepath+=~/.nvim
    source ~/.nvim/vimrc
    EOF

    pip install neovim

Follow the instructions at https://github.com/neovim/neovim/tree/master/contrib/YouCompleteMe

After opening nvim, run ```:PlugInstall``` the first time

Shortcuts

* ,w      save file
* ,nn     open nerdTree
* ,sh     open shell in horizontal split
* ,vsh    open shell in vertical split
* ,o      open buffexplorer
* ,x      open scratch buffer
* ,q      close the buffer, but don't quit
* .       repeat
* u       undo
* <C-R>   repeat redo
* gcc     comment line
* <C-f>   Open a file search (Control-P)