tnoremap <C-h> <C-\><C-n><C-w>h
tnoremap <C-j> <C-\><C-n><C-w>j
tnoremap <C-k> <C-\><C-n><C-w>k
tnoremap <C-l> <C-\><C-n><C-w>l

tnoremap jk <C-\><C-n>

" Open shell
nnoremap <leader>sh :spl term://$SHELL<cr>
nnoremap <leader>vsh :vspl term://$SHELL<cr>
