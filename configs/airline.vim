let g:airline_theme="badwolf"
let g:airline#extensions#tabline#enabled = 1

set laststatus=2
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l
